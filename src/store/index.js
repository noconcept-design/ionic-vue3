import { createStore } from 'vuex'

const store = createStore({
  state() {
    return {
      memories: [
        {
          id: 'm1',
          image: '/assets/1.png',
          title: 'Best trip ever',
          description: 'A journey through the Mandelbrot-Set',
        },
        {
          id: 'm2',
          image: '/assets/7.png',
          title: 'Other Trip i done',
          description: 'The Buddhabrot, it is not the same as Buddha Milk ;-)',
        },
      ],
    }
  },
  mutations: {
    addMemory(state, memoryData) {
      const newMemory = {
        id: new Date().toISOString(),
        title: memoryData.title,
        image: memoryData.imageUrl,
        description: memoryData.description,
      }

      state.memories.unshift(newMemory)
    },
  },
  actions: {
    addMemory(context, memoryData) {
      context.commit('addMemory', memoryData)
    },
  },
  getters: {
    memories(state) {
      return state.memories
    },
    memory(state) {
      return (memoryId) => {
        return state.memories.find((memory) => memory.id === memoryId)
      }
    },
  },
})

export default store
