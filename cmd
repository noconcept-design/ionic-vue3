ionic serve

ionic integrations enable capacitor
npx cap init "Vueionicerinnerungen" "ooo.oje.vierinnerungen"

// first build
ionic build
npx cap add android
npx cap open android

// after modifcations
ionic build
npx cap copy

// if android studio isnt running
npx cap open android